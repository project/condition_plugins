### SUMMARY
This module adds a Condition plugin collection.

### INSTALLATION
Using composer:
```
composer require drupal/condition_plugins --sort-packages
```
No configuration required.

### PLUGIN LIST
- **First request**: If it is the first time the user visits the page.
- **Request parameter**: If current request has the parameters.

### SPONSORS
- [Fundación UNICEF Comité Español](https://www.unicef.es)

### CONTACT
Developed and maintained by Cambrico (http://cambrico.net).

Get in touch with us for customizations and consultancy:
http://cambrico.net/contact

#### Current maintainers:
- Pedro Cambra [(pcambra)](https://www.drupal.org/u/pcambra)
- Manuel Egío [(facine)](https://www.drupal.org/u/facine)
