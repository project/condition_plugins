<?php

namespace Drupal\condition_plugins;

use Drupal\Core\TempStore\PrivateTempStoreFactory as PrivateTempStoreFactoryBase;

/**
 * Creates a PrivateTempStore object for a given collection.
 */
class PrivateTempStoreFactory extends PrivateTempStoreFactoryBase {

}
